function createSet(array) {
    var result = [];
    var char = '';
    
    for (var i = 0; i < array.length; i++) {
        char = array[i];
        if (result.indexOf(char) === -1) {
            result.push(char)
        }
    }
    
    return result;
}

function rndGen() {
    var setNum = []
    var num = 0
    
    while (setNum.length !== 4) {
        num = $jsapi.random(9000) + 1000;
        setNum = createSet(String(num));
    }
    
    return num;
}

function game(inputNum, inputGuess) {
    var guess = inputGuess;

    if (guess == inputNum) {
        $reactions.answer("Правильно! Еще раз хочешь?");
        $reactions.transition("/Правила/Согласен?");
    }
    else {
        var sNum = String(inputNum);
        var sGuess = String(guess);
        var setGuess = createSet(sGuess);
        // пересечение мн-ва цифр сгенерированного числа с мн-вом цифр числа-попытки
        var setInter = [];
        for (var j = 0; j < sNum.length; j++) {
            if (setGuess.indexOf(sNum[j]) !== -1)
                setInter.push(sNum[j]);
        }
        
        if (sGuess.length === 4) {
            if (setGuess.length === 4) {
                
                var bulls = [];
                for (var k = 0; k < sNum.length; k++) {
                    if (sGuess[k] === sNum[k])
                        bulls.push(sGuess[k]);
                }
                // "коровы" как определенное выше пересечение без "быков"
                var cows = [];
                for (var l = 0; l < setInter.length; l++) {
                    if (bulls.indexOf(setInter[l]) === -1)
                        cows.push(setInter[l]);
                }
                
                $reactions.answer(
                    cows.length + " " + $nlp.conform('корова', cows.length) + 
                    ", " + 
                    bulls.length + " " + $nlp.conform('бык', bulls.length)
                );
                
            }
            else $reactions.answer(selectRandomArg( diffNum ));
        }
        else $reactions.answer(selectRandomArg( fourDigits ));
    }
}
