require: slotfilling/slotFilling.sc
  module = sys.zb-common

require: common.js
    module = sys.zb-common
    
require: functions.js

require: my_reactions.js

theme: /

    state: Правила
        q!: $regex</start>
        intent!: /Давай поиграем
        a: Игра "Быки и коровы". Я загадываю четырехзначное число с неповторяющимися цифрами, а тебе нужно отгадать его. Подсказки: "Бык" - правильно угаданная цифра вплоть до позиции, "Корова" - правильно угаданная цифра вне своей позиции. Начнём?
        go!: /Правила/Согласен?

        state: Согласен?

            state: Да
                intent: /Согласие
                go!: /Игра

            state: Нет
                intent: /Несогласие
                a: Так и быть. Захочешь играть - скажи "давай поиграем".

    state: Игра
        script:
            $session.number = rndGen()
        go!: /Проверка
            
    state: Проверка
        intent: /Число
        script:
            game($session.number, $parseTree._Number);
          
    state: NoMatch || noContext = true
        event!: noMatch
        random:
            a: Я не понимаю.
            a: Что-что?
            a: Ничего не пойму!